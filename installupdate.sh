#!/bin/bash

clear

show_main_menu() {
while true; do
clear
echo "#################################################################"
echo "(cpu-cli-installer) >> (april 2024)"
echo "#################################################################"
echo "  ██████ ██████  ██    ██        ██████ ██      ██ "
echo " ██      ██   ██ ██    ██       ██      ██      ██ "
echo " ██      ██████  ██    ██ █████ ██      ██      ██ "
echo " ██      ██      ██    ██       ██      ██      ██ "
echo "  ██████ ██       ██████         ██████ ███████ ██ "
echo "#################################################################"
echo "(cpu-cli-gitlab) >> (https://gitlab.com/manoel-linux1/cpu-cli)"
echo "#################################################################"

if [[ $EUID -ne 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(Superuser privileges or sudo required to execute the script)" 
echo "#################################################################"
exit 1
fi

sudo dnf update
sudo dnf install iputils -y
echo "#################################################################"
sudo pacman -Sy
sudo pacman -S iputils -y
echo "#################################################################"
sudo xbps-install -Sy
sudo xbps-install -S inetutils-ping -y
echo "#################################################################"
sudo apt-get update
sudo apt-get install --no-install-recommends inetutils-ping -y
echo "#################################################################"

clear

echo "#################################################################"
echo "(1)> (Install) >> (the CPU-CLI version of Void-Linux)"
echo "(2)> (Install) >> (the CPU-CLI version of Ubuntu/Debian)"
echo "(3)> (Install) >> (the CPU-CLI version of Arch-Artix-Manjaro)"
echo "(4)> (Install) >> (the CPU-CLI version of FreeBSD)"
echo "(5)> (Install) >> (the CPU-CLI version of Fedora (Experimental)"
echo "(6)> (Install) >> (the CPU-CLI version of Slackware (Experimental)"
echo "(7)> (Exit)"
echo "#################################################################"

read -p "(Enter your choice) >> " choice
echo "#################################################################"

case $choice in
1)
show_void-linux
;;
2)
show_ubuntu-debian
;;
3)
show_arch-artix-manjaro
;;
4)
show_freebsd
;;
5)
show_fedora
;;
6)
show_slackware
;;
7)
exit 0
;;
*)
echo "(Invalid choice. Please try again)"
echo "#################################################################"
sleep 2
;;
esac
done
}

show_void-linux() {
while true; do
clear
if [ ! -x /bin/xbps-install ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "(Checking for updates in Void Linux)" 
echo "#################################################################"
sudo xbps-install -Sy
sudo xbps-install -S glxinfo dmidecode unzip binutils tar curl xbps usbutils grep gawk sed lm_sensors dialog hdparm mokutil -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo xbps-install -Sy
sudo xbps-install -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

sudo rm /usr/bin/cpu-t

sudo rm /usr/bin/cpu-cli

clear

sudo cp cpu-cli /usr/bin/

sudo chmod +x /usr/bin/cpu-cli

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(To use CPU-CLI, execute the following command >> cpu-cli)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_ubuntu-debian() {
while true; do
clear
if [ ! -x /bin/apt ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Ubuntu/Debian)" 
echo "#################################################################"
sudo apt-get update
sudo apt-get install --no-install-recommends mesa-utils dmidecode unzip binutils tar curl usbutils grep gawk sed lm-sensors dialog hdparm mokutil -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo apt-get update
sudo apt-get upgrade -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

sudo rm /usr/bin/cpu-t

sudo rm /usr/bin/cpu-cli

clear

sudo cp cpu-cli /usr/bin/

sudo chmod +x /usr/bin/cpu-cli

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(To use CPU-CLI, execute the following command >> cpu-cli)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_arch-artix-manjaro() {
while true; do
clear
if [ ! -x /bin/pacman ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Arch/Artix/Manjaro)" 
echo "#################################################################"
sudo pacman -Sy
sudo pacman -S mesa-utils dmidecode mesa-demos unzip binutils tar curl usbutils grep gawk sed lm_sensors dialog hdparm mokutil -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo pacman -Sy
sudo pacman -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

sudo rm /usr/bin/cpu-t

sudo rm /usr/bin/cpu-cli

clear

sudo cp cpu-cli /usr/bin/

sudo chmod +x /usr/bin/cpu-cli

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(To use CPU-CLI, execute the following command >> cpu-cli)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi
break
done

echo "#################################################################"
}

show_freebsd() {
while true; do
clear

if [ ! -x /usr/sbin/pkg ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in FreeBSD)" 
echo "#################################################################"
sudo pkg update
sudo pkg install dmidecode mesa-demos unzip curl usbutils gawk freecolor
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo pkg update
sudo pkg upgrade -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

sudo rm /usr/bin/cpu-cli-freebsd

clear

sudo cp cpu-cli-freebsd /usr/bin/

sudo chmod +x /usr/bin/cpu-cli-freebsd

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(To use CPU-CLI, execute the following command >> cpu-cli-freebsd)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_fedora() {
while true; do
clear
if [ ! -x /bin/dnf ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Fedora)" 
echo "#################################################################"
sudo dnf update
sudo dnf install mesa-demos dmidecode unzip binutils tar curl usbutils grep gawk sed lm_sensors dialog hdparm mokutil -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo dnf update
sudo dnf upgrade -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

sudo rm /usr/bin/cpu-t

sudo rm /usr/bin/cpu-cli

clear

sudo cp cpu-cli /usr/bin/

sudo chmod +x /usr/bin/cpu-cli

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(To use CPU-CLI, execute the following command >> cpu-cli)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_slackware() {
while true; do
clear
if [ ! -x /usr/sbin/slackpkg ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi

clear

sudo rm /usr/bin/cpu-t

sudo rm /usr/bin/cpu-cli

clear

sudo cp cpu-cli /usr/bin/

sudo chmod +x /usr/bin/cpu-cli

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(To use CPU-CLI, execute the following command >> cpu-cli)"
echo "#################################################################"
echo "(Please execute this command: slackpkg install binutils tar curl dmidecode usbutils grep gawk sed lm_sensors dialog hdparm)"
echo "(if you have already installed it; otherwise, do not execute)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_main_menu