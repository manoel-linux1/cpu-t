# CPU-CLI

####################################

# For Linux

- cpu-cli-version: oct 2023

- build-latest: 0.3.1

# For FreeBSD

- cpu-cli-freebsd-version: oct 2023

- build-latest: 0.0.1

- On FreeBSD, install bash to have no issues.

- To resolve the issue of the CPU-CLI error, even with bash, use the following command: `sudo ln -s /usr/local/bin/bash /bin/`

####################################

- Support for the distro: Void-Linux/Ubuntu/Debian/Arch/Artix/Manjaro/FreeBSD/Fedora (Experimental)/Slackware (Experimental)

- If you are using a distribution based on Ubuntu, Debian, or Arch, the script will work without any issues.

- from version 0.0.9 now supports Void Linux and Ubuntu/Debian, as well as Arch/Artix/Manjaro within the installer without the need to go to another link

- Starting from version 0.0.6, executing CPU-CLI as a superuser or with sudo privileges has been blocked. It can only be executed without superuser or sudo privileges

- CPU-CLI is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with CPU-CLI, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of CPU-CLI to add additional features.

## Installation

- To install CPU-CLI, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/cpu-cli.git

# 2. To install the CPU-CLI script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# 3. Execute the CPU-CLI script

For Linux

- `cpu-cli`

For FreeBSD

- `cpu-cli-freebsd`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The CPU-CLI project is currently in development. The latest stable version is 0.3.1-Linux/0.0.1-FreeBSD. We aim to provide regular updates and add more features in the future.

# License

- CPU-CLI is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of CPU-CLI.
